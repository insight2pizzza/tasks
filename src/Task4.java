/**
 * 1.3. Удаление из массива
 * <p>
 * Задача:
 * <p>
 * Дан массив целых чисел и ещё одно целое число. Удалите все вхождения этого числа из массива (пропусков быть не должно).
 */
public class Task4 {

    public static int[] deleteElement() {
        int arr[] = {0, 1, 2, 2, 3, 0, 4, 2};
        int num = 2;
        int counter = 0;
        int[] temp;

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == num) {
                counter++;
            }
        }
        temp = new int[arr.length - counter];
        int j = 0;
        for (int i = 0; i < arr.length - 1; i++) {
            if (arr[i] != num) {
                temp[j] = arr[i];
                j++;
            }
        }

        return temp;
    }

    public static void main(String[] args) {
        int[] result = deleteElement();
        for (int i : result) {
            System.out.print(i + " ");
        }
    }
}
