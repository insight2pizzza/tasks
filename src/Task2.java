/**
 * 1.1. Реализуйте алгоритм сортировки пузырьком для сортировки массива
 */
public class Task2 {

    private static int[] array = new int[10];

    public static int[] fillArray() {
        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * 100);
        }
        return array;
    }

    public static int[] bubbleSort(int[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            int temp;
            for (int j = 0; j < array.length - i - 1; j++) {
                if (array[j] > array[j + 1]) {
                    temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }
        return array;
    }

    public static void main(String[] args) {
        int[] arr = fillArray();
        for (int i : arr) {
            System.out.print(i + " ");
        }
        System.out.println();

        int[] arrs = bubbleSort(arr);
        for (int i : arrs) {
            System.out.print(i + " ");
        }
    }
}
