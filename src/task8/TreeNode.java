package task8;

public class TreeNode {
    public Integer data;
    public TreeNode leftNode;
    public TreeNode rightNode;

    public TreeNode(Integer data) {
        this.data = data;
        leftNode = null;
        rightNode = null;
    }
}
