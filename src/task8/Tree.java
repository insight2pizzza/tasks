package task8;

public class Tree {
    public TreeNode treeNode;

    public TreeNode addElements(TreeNode treeNode, int value) {
        if (treeNode == null) {
            return new TreeNode(value);
        }
        if (value > treeNode.data) {
            treeNode.rightNode = addElements(treeNode.rightNode, value);
        }
        if (value < treeNode.data) {
            treeNode.leftNode = addElements(treeNode.leftNode, value);
        } else {
            return treeNode;
        }
        return treeNode;
    }

    public void add(int value) {
        treeNode = addElements(treeNode, value);
    }

    public int innerSearchNode(TreeNode treeNode, int searchData) {
        if (treeNode == null) {
            return -1;
        }
        if (treeNode.data == searchData) {
            return treeNode.data;
        }

        return searchData < treeNode.data
                ? innerSearchNode(treeNode.leftNode, searchData)
                : innerSearchNode(treeNode.rightNode, searchData);
    }

    public int searchNode(int searchData) {
        return innerSearchNode(treeNode, searchData);
    }
}
