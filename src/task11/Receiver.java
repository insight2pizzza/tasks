package task11;

public interface Receiver extends User, Message{
    void getMessage();
}
