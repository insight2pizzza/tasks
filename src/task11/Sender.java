package task11;

import java.util.List;

public interface Sender extends User, Message{
    void sendTo(String phoneNumber);
    void sendTo(List<String> phoneNumbers);
}
