package task5;

/**
 * 2.0. Проектирование и создание класса, описывающего вектор
 * <p>
 * Задача:
 * <p>
 * Создайте класс, который описывает вектор (в трёхмерном пространстве).
 * <p>
 * У него должны быть:
 * <p>
 * конструктор с параметрами в виде списка координат x, y, z
 * метод, вычисляющий длину вектора. Корень можно посчитать с помощью Math.sqrt():
 * метод, вычисляющий скалярное произведение:
 * метод, вычисляющий векторное произведение с другим вектором:
 * метод, вычисляющий угол между векторами (или косинус угла): косинус угла между векторами равен скалярному произведению векторов, деленному на произведение модулей (длин) векторов:
 * методы для суммы и разности:
 * <p>
 * статический метод, который принимает целое число N, и возвращает массив случайных векторов размером N.
 * <p>
 * Если метод возвращает вектор, то он должен возвращать новый объект, а не менять базовый. То есть, нужно реализовать шаблон "Неизменяемый объект"
 */
public class Task5 {
    public static void main(String[] args) {
        Vector vector = new Vector(2, 4, 6);
        Vector vector1 = new Vector(3, 5, 7);
        System.out.println(vector.vectorHeight(vector));
        System.out.println(vector.dotProduct(vector, vector1));
        System.out.print(vector.crossProduct(vector, vector1));
        System.out.println();
        System.out.println(vector.sumVectors(vector, vector1));
        System.out.print(vector.diffVectors(vector,vector1));
        System.out.println();
        Vector[] vectors = vector.randVectors(4);
        System.out.println(vectors[0]);
        System.out.println(vectors[1]);
        System.out.println(vectors[2]);
        System.out.println(vectors[3]);
    }
}
