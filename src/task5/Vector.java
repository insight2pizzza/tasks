package task5;

public class Vector {
    private int x, y, z;

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getZ() {
        return z;
    }

    public void setZ(int z) {
        this.z = z;
    }

    public Vector(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Vector() {

    }

    public double vectorHeight(Vector vector) {
        return Math.sqrt(Math.pow(vector.x, 2) + Math.pow(vector.y, 2) + Math.pow(vector.z, 2));
    }

    public double dotProduct(Vector vector, Vector vector1) {
        return vector.x * vector1.x + vector.y * vector1.y + vector.z * vector1.z;
    }

    public Vector crossProduct(Vector vector, Vector vector1) {
        return new Vector(vector.y * vector1.z - vector.z * vector1.y,
                vector.z * vector1.x - vector.x * vector1.z,
                vector.x * vector1.y - vector.y * vector1.x);
    }

    public double corner(Vector vector, Vector vector1) {
        double dotProduct = dotProduct(vector, vector1);
        double modulesResult = vectorHeight(vector) * vectorHeight(vector1);
        return dotProduct / modulesResult;
    }

    public Vector sumVectors(Vector vector, Vector vector1) {
        return new Vector(vector.x + vector1.x, vector.y + vector1.y, vector.z + vector1.z);
    }

    public Vector diffVectors(Vector vector, Vector vector1) {
        return new Vector(vector.x - vector1.x, vector.y - vector1.y, vector.z - vector1.z);
    }

    public static Vector[] randVectors(int n) {
        Vector[] vectors = new Vector[n];
        for (int i = 0; i < vectors.length; i++) {
            vectors[i] = new Vector((int) (Math.random() * 100), (int) (Math.random() * 100), (int) (Math.random() * 100));
        }
        return vectors;
    }

    @Override
    public String toString() {
        return "Vector{" +
                "x=" + x +
                ", y=" + y +
                ", z=" + z +
                '}';
    }
}
