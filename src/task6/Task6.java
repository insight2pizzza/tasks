/**
 * 2.1. Генерация случайного элемента с весом
 * <p>
 * Задача:
 * <p>
 * Напишите класс, конструктор которого принимает два массива: массив значений и массив весов значений.
 * Класс должен содержать метод, который будет возвращать элемент из первого массива случайным образом, с учётом его веса.
 * Пример:
 * Дан массив [1, 2, 3], и массив весов [1, 2, 10].
 * В среднем, значение «1» должно возвращаться в 2 раза реже, чем значение «2» и в десять раз реже, чем значение «3».
 */
package task6;

public class Task6 {

    public static void main(String[] args) {
        int[] values = new int[]{1,2,3};
        int[] weight = new int[]{2,4,1};
        RandomElemGenerator randomElemGenerator = new RandomElemGenerator(values, weight);
        System.out.println(randomElemGenerator.getRandom());
    }
}
