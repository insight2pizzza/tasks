package task6;

public class RandomElemGenerator {
    private int[] values;
    private int[] weights;
    private int[] summaryValues;

    public RandomElemGenerator(int[] values, int[] weights) {
        int sum = 0;
        for (int weight : weights) {
            sum += weight;
        }

        summaryValues = new int[sum];
        int cursor = 0;

        for(int i = 0; i < weights.length; i++){
            for(int j = 0; j < weights[i]; j++){
                summaryValues[cursor++] = values[i];
            }
        }
    }

    public int getRandom() {
        int random = (int) (Math.random() * ( summaryValues.length - 1));
        return summaryValues[random];
    }


}
