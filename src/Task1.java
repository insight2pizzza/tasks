import java.util.Arrays;

/**
 * 1.0. Максимальное, минимальное и среднее значение
 * <p>
 * Задача:
 * <p>
 * Заполните массив случайным числами и выведите максимальное, минимальное и среднее значение.
 * <p>
 * Для генерации случайного числа используйте метод Math.random().
 */
public class Task1 {

    private static int[] array = new int[10];

    public static int[] fillArray() {
        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * 100);
        }
        return array;
    }

    public static int findMin(int[] array) {
        int min = array[0];
        for (int i = 0; i < array.length; i++) {
            if (min > array[i]) {
                min = array[i];
            }
        }

        return min;
    }

    public static int findAverage(int[] array) {
        int avg = 0;
        for (int i = 0; i < array.length; i++) {
            avg += array[i]/array.length;
        }
        return avg;
    }

    public static int findMax(int[] array) {
        int max = array[0];
        for (int i = 0; i < array.length; i++) {
            if (max < array[i]) {
                max = array[i];
            }
        }

        return max;
    }

    public static void main(String[] args) {
        int[] arr = fillArray();
        for (int i : arr) {
            System.out.print(i + " ");
        }
        System.out.println();
        System.out.println(findMin(arr));
        System.out.println(findMax(arr));
        System.out.println(findAverage(arr));
    }
}
