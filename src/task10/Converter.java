package task10;

public interface Converter {
    double getConvertedValue(double value);
}
