package task10;

public class KelvinConverter implements Converter{

    public KelvinConverter() {
    }

    @Override
    public double getConvertedValue(double value) {
        return value + 273.15;
    }
}
