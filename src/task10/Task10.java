/**
 * 6.0. Конвертер температур
 *
 * Задача:
 *
 * Напишите класс BaseConverter для конвертации из градусов по Цельсию в
 * Кельвины Фаренгейты, и так далее. У метода должен быть метод convert, который
 * и делает конвертацию.
 */
package task10;

public class Task10 {
    public static void main(String[] args) {
        double temperature = 27.3;
        CelsiusConverter celsiusConverter = new CelsiusConverter();
        FahrenheitConverter fahrenheitConverter = new FahrenheitConverter();
        KelvinConverter kelvinConverter = new KelvinConverter();
        System.out.println(kelvinConverter.getConvertedValue(temperature));
        System.out.println(fahrenheitConverter.getConvertedValue(temperature));
        System.out.println(celsiusConverter.getConvertedValue(temperature));
    }
}
