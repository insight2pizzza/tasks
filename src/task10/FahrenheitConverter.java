package task10;

public class FahrenheitConverter implements Converter{
    public FahrenheitConverter() {
    }

    @Override
    public double getConvertedValue(double value) {
        return value * 1.8 + 32;
    }
}
