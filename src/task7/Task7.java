/**
 * 3.0. Двоичный поиск
 * <p>
 * Задача:
 * <p>
 * Напишите метод, который проверяет, входит ли в массив заданный элемент или нет.
 * Используйте перебор и двоичный поиск для решения этой задачи.
 * Сравните время выполнения обоих решений для больших массивов (например, 100000000 элементов).
 */
package task7;

import java.util.Arrays;

public class Task7 {
    public static void main(String[] args) {
        BinarySearch binarySearch = new BinarySearch();
        int[] temp = binarySearch.fillRandom();
        long startTime = System.currentTimeMillis();
        System.out.println(binarySearch.findElemByEnumeration(2988756));
        long endTime = System.currentTimeMillis();
        System.out.println("Total execution time: " + (endTime-startTime) + "ms");
        Arrays.sort(temp);
        long startTimeBinnary = System.currentTimeMillis();
        System.out.println(binarySearch.binarySearch(temp, 2988756, 0, temp.length));
        long endTimeBinnary = System.currentTimeMillis();
        System.out.println("Total execution time: " + (endTimeBinnary - startTimeBinnary) + "ms");
    }
}
