package task7;

public class BinarySearch {
    private int[] arr = new int[100000000];

    public BinarySearch() {
    }

    public int[] fillRandom() {
        for (int i = 0; i < arr.length; i++) {
            arr[i] = (int) (Math.random() * 100000000);
        }
        return arr;
    }

    public int findElemByEnumeration(int searchElem) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == searchElem) {
                return arr[i];
            }
        }
        return -1;
    }

    public int binarySearch(int[] sorted, int searchElem, int low, int high) {
        int mid = (low + high) / 2;

        if (high < low) {
            return -1;
        }

        if (searchElem == sorted[mid]) {
            return sorted[mid];
        } else if (searchElem < sorted[mid]) {
            return binarySearch(
                    sorted, searchElem, low, mid - 1);
        } else {
            return binarySearch(
                    sorted, searchElem, mid + 1, high);
        }
    }
}
