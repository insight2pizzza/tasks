/**
 * 1.2. Поиск простых чисел
 * <p>
 * Задача:
 * <p>
 * Напишите программу, которая выводит на консоль простые числа в промежутке от [2, 100].
 * Используйте для решения этой задачи оператор "%" (остаток от деления) и циклы.
 */
public class Task3 {

    public static void soutSimpleNumbers() {
        for (int i = 2; i <= 100; i++) {
            boolean isSimple = true;
            for (int j = 2; j < i; j++) {
                if (i % j == 0) {
                    isSimple = false;
                    break;
                }
            }
            if (isSimple) {
                System.out.print(i + " ");
            }
        }
    }

    public static void main(String[] args) {
        soutSimpleNumbers();
    }
}
